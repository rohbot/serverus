import pyttsx

import redis
import threading
import random
import struct


class Listener(threading.Thread):
    def __init__(self, r, channels):
        threading.Thread.__init__(self)
        self.redis = r
        self.pubsub = self.redis.pubsub()
        self.pubsub.subscribe(channels)
        self.engine = pyttsx.init()
    
    def work(self, item):
        phrase = item['data']
        print item['channel'], ":", phrase
        self.engine.say(phrase)
        self.engine.runAndWait()

    
    def run(self):
        for item in self.pubsub.listen():
            if item['data'] == "KILL":
                self.pubsub.unsubscribe()
                print self, "unsubscribed and finished"
                break
            else:
                self.work(item)

engine = pyttsx.init()
engine.say("hello")
engine.runAndWait()
