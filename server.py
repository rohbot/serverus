from flask import Flask, json, request, redirect, render_template, make_response, url_for, session, abort, jsonify
from flaskext.mysql import MySQL
from redis import Redis
from hashlib import md5
import sys
import json
import zmq
import cPickle as pickle
import MySQLdb as mdb
import struct

context = zmq.Context(1)
socket = context.socket(zmq.PUB)
socket.connect("tcp://localhost:8888")
		


app = Flask(__name__)
mysql = MySQL()
app.config["MYSQL_DATABASE_USER"] = "spg"
app.config["MYSQL_DATABASE_PASSWORD"] = "spg"
app.config["MYSQL_DATABASE_DB"] = "spg"
mysql.init_app(app)
redis = Redis()
app.secret_key = 'JVg\x063S\xac(\xab\xb6\x1euu\xf8\xf6?Y1g\xd1\xc5e\xe7\x1e'

class RunningTime:
	
	def __init__(self):
		self.id = 0
		
	
	def parseJson(self,json):
		try:
			self.run_id = json['id']
			self.distance = json['distance']
			self.time = json['time']
			self.timeString = json['timeString']
			self.lat = json['lats']
			self.lng = json['longs']
			return True
		except:
			return False

@app.route('/favicon.ico')
def favicon():
	return ''

# @app.route('/test')
# def test():
# 	return 'this is a test'


@app.route('/git_hook/<repo_name>', methods=['POST'])
def git_hook(repo_name):
	data = request.form.get('payload')	
	#print request.form
	print data
	redis.publish('git-hook', repo_name) 
	redis.set('commit-data:'+repo_name, data)
	return "ok"

@app.route('/<phrase>')
def say_phrase(phrase):
	print "received: " , phrase
	data =  { 'said': phrase }
	redis.publish('web-feed', phrase) 
	socket.send("web-feed " + str(phrase))	
	return jsonify(**data)

@app.route('/time/add', methods=['POST'])
def add_time():
	
	time =  RunningTime()
	if not time.parseJson(request.json):
		return "Error!"		
	#print request.json	
	cur = mysql.get_db().cursor()
	#convert lat/lng to binary
	lats = struct.pack('%sd' % len(time.lat), *time.lat)
	lngs = struct.pack('%sd' % len(time.lng), *time.lng)
	cur.execute("insert into running_time(run_id,time,timeString,distance,lat,lng,waypoints) values (%s, %s, %s, %s, %s, %s, %s)", (time.run_id, time.time, time.timeString, time.distance, lats, lngs, len(time.lat))) 
	_id = cur.lastrowid
	data = {"id":_id}
	mysql.get_db().commit()	
		
	return jsonify(**data)

@app.route('/time/get')
def get_times():
	cur = mysql.get_db().cursor((mdb.cursors.DictCursor))
	cur.execute("""SELECT id, run_id, timeString, time, distance from running_time order by id desc limit 50""")
		
	rows = cur.fetchall()
	
	data = {"data":rows}	
					
		
	return jsonify(**data)
	

@app.route('/time/latlng/<id>')
def get_latlng(id):
	cur = mysql.get_db().cursor((mdb.cursors.DictCursor))
	cur.execute("SELECT run_id, lat, lng,waypoints from running_time where id = %s", id)
		
	rows = cur.fetchall()
	latlng = []
	
	for row in rows:
		raw_lat = row['lat']
		raw_lng = row['lng']
		waypoints = row['waypoints'] 
		lat = list(struct.unpack('%sd' % waypoints,raw_lat))
		lng = list(struct.unpack('%sd' % waypoints,raw_lng))
	for i in range(waypoints):
		latlng.append([lat[i],lng[i]])
	
	data = {"latlng":latlng}	
					
		
	return jsonify(**data)

@app.route('/map')
def map():
	return render_template('map.html')	
	 	

@app.route('/')
def hello_world():
	name = 	request.args.get('name')
	
	if name is None:
		return render_template('index.html')	
	
	data =  { 'hello': name } 
	
	return jsonify(**data)



if __name__ == '__main__':


	app.debug = True
	app.run('0.0.0.0',8080)
