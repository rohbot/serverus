import redis
import threading
import subprocess
class Listener(threading.Thread):
    def __init__(self, r, channels):
        threading.Thread.__init__(self)
        self.redis = r
        self.pubsub = self.redis.pubsub()
        self.pubsub.subscribe(channels)
        
    
    def work(self, item):
		phrase = item['data']
		print item['channel'], ":", phrase
		if phrase == 'serverus':
			print "git pull from master"
			output = subprocess.check_output(['./deploy.sh'])
			print output
		elif phrase == 'stopwatch':
			print "Building StopWatch.apk"
			output = subprocess.check_output(['./build_stopwatch.sh'])
			print output

	    
    def run(self):
        for item in self.pubsub.listen():
            if item['data'] == "KILL":
                self.pubsub.unsubscribe()
                print self, "unsubscribed and finished"
                break
            else:
                self.work(item)

if __name__ == "__main__":
	r = redis.Redis()
	client = Listener(r, ['git-hook'])
	client.start()
