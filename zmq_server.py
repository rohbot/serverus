import zmq
context = zmq.Context(1)
publisher = context.socket(zmq.PUB)
publisher.bind("tcp://*:8889")
subscriber = context.socket(zmq.SUB)
subscriber.bind("tcp://*:8888")

subscriber.setsockopt(zmq.SUBSCRIBE, "web-feed")
print "Listening on port 8888"
while True:
	msg = subscriber.recv()
	print msg
	publisher.send(msg)
	





